import json
import requests

# user defined - START
key = 'YOUR_GODADDY_KEY_GOES_HERE'
secret = 'YOUR_GODADDY_SECRET_GOES_HERE'
requestDomain = 'YOUR_GODADDY_DOMAIN_HERE' # ex) notreal.com
# user defined - END

# static - START 
rootUrl = 'https://api.godaddy.com'
recordType = 'A'
recordName = '@'
requestUrl = "%s/v1/domains/%s/records/%s/%s" % (rootUrl, requestDomain, recordType, recordName)
requestHeaders = {
	'Content-Type': 'application/json',
	'Authorization': ("sso-key %s:%s" % (key, secret))
}
# static - END

public_ip = requests.get('http://ip.42.pl/raw').text
records = requests.get(
	requestUrl,
	headers=requestHeaders
).json()

if records[0]['data'] != public_ip:
	print "UPDATING TO %s" % (public_ip)
	records[0]['data'] = public_ip

	requests.put(
		requestUrl,
		data=json.dumps(records),
		headers=requestHeaders
	)
else:
	print "%s IS UP TO DATE" % (public_ip)