If you want a GoDaddy domain name and you don't have a static IP address, you can use this small script to update the A record of your domain. This can be executed inside a CRON job to make sure your domain is always up to date.

# Install python dependencies
* `pip install json`
* `pip install requests`

# Run the script
* `python godaddy_update.py`